try {
  if (typeof Cc === "undefined") var Cc = Components.classes;
  if (typeof Ci === "undefined") var Ci = Components.interfaces;
//  if (typeof Cr === "undefined") var Cr = Components.results;
} catch (e) {}

var { ExtensionCommon } = ChromeUtils.import("resource://gre/modules/ExtensionCommon.jsm");
var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");

// var FileUtils = Cu.import("resource://gre/modules/FileUtils.jsm").FileUtils;

var filePicker = class extends ExtensionCommon.ExtensionAPI {
  getAPI(context) {
    return {
      filePicker: {
        async browseForFolder(folderPath, windowTitle, windowId, optionsDebug) {
          if(optionsDebug)
            console.debug("AEC: folderPath: " + folderPath);
          return new Promise(resolve => {
            let window = context.extension.windowManager.get(windowId, context).window;
            let nsIFilePicker = Ci.nsIFilePicker;
            let fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

            try {
              fp.init(window, windowTitle, Ci.nsIFilePicker.modeGetFolder);
              try {
                if(optionsDebug)
                  console.debug("AEC: folderPath: " + folderPath);
                if (folderPath) {
                  let localFile = Cc["@mozilla.org/file/local;1"].createInstance(
                      Ci.nsIFile);
                  localFile.initWithPath(folderPath);
                  fp.displayDirectory = localFile;
                }
              } catch (e) {}
              fp.open(r => {
                if (r !== Ci.nsIFilePicker.returnOK || !fp.file) {
                  resolve();
                }
                else {
                  resolve(fp.file.path);
                }
              });
            } catch (e) { resolve(); }
          })
        }
      }
    }
  }
};
