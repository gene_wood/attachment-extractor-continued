var options = {};

function reloadOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    if (res[id] != undefined)
      options[id] = res[id];
    else
      options[id] = DefaultOptions[id];
  }, defaultError);
}

async function reloadAllOptions() {
  await reloadOption("debug");
  await reloadOption("autoDetach");
  await reloadOption("autoDetachTaggedOnly");
  await reloadOption("autoDetachTriggerTag");
}

async function optionsInit() {
  await reloadAllOptions();
}

messenger.storage.onChanged.addListener(optionsInit);
optionsInit();