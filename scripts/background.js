// Add eventListener for onClicked on ALL menu entries
// The different elements must be differed by elements ID in the called function
browser.menus.onClicked.addListener(onContextMenuClicked);
async function onContextMenuClicked(info, tab) {
  consoleDebug("AEC: Menu ID clicked = " + info.menuItemId);

  if (info.menuItemId != "aec_extractToBrowse")
    return;

  // Load the list of selected messages in message/thread pane
  //
  // The following code line would exclusively return prior (per left click) 
  // selected messages. Opening the context menu on an other message 
  // would NOT return this message for the operations.
  //
  // messages = await browser.mailTabs.getSelectedMessages(tab.id);
  //
  // This following used code line does return the expected message(s) for 
  // the operations.
  //
  messages = await info.selectedMessages;

  collectMessagesToDetach(folder=[], messages, false);
}

// Add eventListener for onClicked on the toolbar button
browser.browserAction.onClicked.addListener(onToolbarButtonClicked);
async function onToolbarButtonClicked(tab, info) {
  consoleDebug("AEC: Toolbar button clicked");
  consoleDebug("AEC: tab.id = " + tab.id);
  consoleDebug("AEC: info.modifiers = " + info.modifiers);

  if (tab.id == 1) {
    // Load the list of selected messages in message/thread pane
    messages = await browser.mailTabs.getSelectedMessages(tab.id);
  }
  else {
    // Get the displayed message; because of being only one message,
    // we need to create an message.message object to be consistent for later processing
    messages = { messages: await browser.messageDisplay.getDisplayedMessages(tab.id) };
  }

  consoleDebug("AEC: messages = " + messages);
  if (messages.messages)
    consoleDebug("AEC: message count = " + messages.messages.length);
  else if (messages.length) {
    consoleDebug("AEC: message count = " + messages.length);
  }

  collectMessagesToDetach(folder=[], messages, false);
}

// Add eventListener for onClicked on the header button
browser.messageDisplayAction.onClicked.addListener(onHeaderButtonClicked);
async function onHeaderButtonClicked(tab, info) {
  consoleDebug("AEC: Header button clicked");
  consoleDebug("AEC: tab.id = " + tab.id);
  consoleDebug("AEC: info.modifiers = " + info.modifiers);

  // Get the displayed message; because of being only one message,
  // we need to create an message.message object to be consistent for later processing
  messages = { messages: await browser.messageDisplay.getDisplayedMessages(tab.id) };

  consoleDebug("AEC: messages = " + messages);
  consoleDebug("AEC: message count = " + messages.length);

  collectMessagesToDetach(folder=[], messages, false);
}

// Add eventListener for onNewMailReceived for automatic detach option
messenger.messages.onNewMailReceived.addListener(async (folder, messages) => {
  consoleDebug("AEC: onNewMailReceived is fired: ", folder, messages);

  // If AutoDetach is disabled, return
  if (!options.autoDetach)
    return;

  consoleDebug("AEC: options.autoDetach is enabled");

  // check for account type and return (do nothing), if it is a rss or news account
  let account = await messenger.accounts.get(folder.accountId);
  consoleDebug("AEC: account type = " + account.type);
  if (account.type == "news" || account.type == "rss")
    return;

  consoleDebug("AEC: Folder type = " + folder.type);

  // Do not automatically detach from new messages in the following special folders
  if ((folder.type == "drafts") || 
      (folder.type == "template") || 
      (folder.type == "sent") || 
      (folder.type == "junk") || 
      (folder.type == "trash")) {
    consoleDebug("AEC: Do not automatically detach from new messages in the special folders: drafts, template, sent, junk and trash: " + folder.type);
    return;
  }

  for (const message of messages.messages) {
    consoleDebug("AEC: message.id: " + message.id + "\n" + 
                  "AEC: folder: " + folder.name + "\n" +
                  "AEC: subject: " + message.subject + "\n" +
                  "AEC: tags: " + message.tags);
  }

  collectMessagesToDetach(folder, messages, true);
});

async function collectMessagesToDetach(folder, currentPage, autoDetach) {
  var allMessages = [];
  // Helper inline function for getting attachment details from a message
  const createMessage = async function(message) {
    const attachments = await browser.messages.listAttachments(message.id);
    return {
      id: message.id,
      account: message.folder.accountId,
      folder: message.folder.path,
      attachments: attachments
    };
  }

  // Iterate through the messages
  consoleDebug("AEC: Inerate through: currentPage.id = " + currentPage.id)
  for (let m of currentPage.messages) {
    consoleDebug("AEC: Iterate through: message.id: " + m.id + "\n" + 
                  "AEC: Iterate through: folder: " + folder.name + "\n" + 
                  "AEC: Iterate through: subject: " + m.subject + "\n" +
                  "AEC: Iterate through: tags: " + m.tags);

    if ((!options.autoDetachTaggedOnly) || (!autoDetach)) {
      consoleDebug("AEC: Iterate through: Manual process or _No_ trigger tag is necessary");
      allMessages.push(await createMessage(m));
      continue;
    } else {
      // A trigger tag is necessary, check every msg for this tag
      consoleDebug("AEC: Iterate through: The trigger tag '" + options.autoDetachTriggerTag + "' is necessary");
      // Check for the trigger tag and check junk status == false
      if ((m.tags.includes(options.autoDetachTriggerTag)) && (m.junk == false)) {
        consoleDebug("AEC: Iterate through: the necessary tag is included");
        allMessages.push(await createMessage(m));
      }
    }
  }
  // As long as there is an ID, more pages can be fetched
  while (currentPage.id) {
    currentPage = await browser.messages.continueList(currentPage.id);
    consoleDebug("AEC: while-loop: currentPage.id = " + currentPage.id)
    for (let m of currentPage.messages) {
      consoleDebug("AEC: while-loop: message.id: " + m.id + "\n" + 
                    "AEC: while-loop: folder: " + folder.name + "\n" + 
                    "AEC: while-loop: subject: " + m.subject + "\n" +
                    "AEC: while-loop: tags: " + m.tags);

      if ((!options.autoDetachTaggedOnly) || (!autoDetach)) {
        consoleDebug("AEC: while-loop: Manual process or _No_ trigger tag is necessary");
        allMessages.push(await createMessage(m));
        continue;
      } else {
        // A trigger tag is necessary, check every msg for this tag
        consoleDebug("AEC: while-loop: The trigger tag '" + options.autoDetachTriggerTag + "' is necessary");
        // Check for the trigger tag and check junk status == false
        if ((m.tags.includes(options.autoDetachTriggerTag)) && (m.junk == false)) {
          consoleDebug("AEC: while-loop: the necessary tag is included");
          allMessages.push(await createMessage(m));
        }
      }
  
    }
  }

  // Call Experiment API to detach attachments from selected messages
  consoleDebug("AEC: allMessages[].length: " + allMessages.length );
  if (allMessages.length > 0)
    await browser.attachmentExtractorContinuedApi.detachAttachmentsFromSelectedMessages(allMessages, options.debug);
}
