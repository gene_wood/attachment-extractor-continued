// Create context menu entry in message pane
browser.menus.create({
  id: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractMessagesTo_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_extractToBrowse",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractToBrowse_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  type: "separator",
  parentId: "aec_extractMessagesTo",
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_extractToDefault",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractToDefault_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_extractToFavoritefolders",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractToFavoritefolders_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_extractToMRUfolders",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractToMRUfolders_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_extractToSuggestfolders",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("extractToSuggestfolders_label"),
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  type: "separator",
  parentId: "aec_extractMessagesTo",
  contexts: ["message_list"]
}, onCreated);

browser.menus.create({
  id: "aec_justDeleteAttachments",
  parentId: "aec_extractMessagesTo",
  title: browser.i18n.getMessage("justDeleteAttachments_label"),
  contexts: ["message_list"]
}, onCreated);

/* *************************************************************************
// Do we really want these additional complex UI elements?
// Due to the fact, that users can loop over multiple folders with the help 
// of virtual folders, the following UI elements are disabled (and not yet 
// functional implemented at all).

// Create context menu entry in folder pane
browser.menus.create({
  id: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractFoldersTo_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_extractFoldersToBrowse",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractToBrowse_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  type: "separator",
  parentId: "aec_extractFoldersTo",
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_extractFoldersToDefault",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractToDefault_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_extractFoldersToFavoritefolders",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractToFavoritefolders_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_extractFoldersToMRUfolders",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractToMRUfolders_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_extractFoldersToSuggestfolders",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("extractToSuggestfolders_label"),
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  type: "separator",
  parentId: "aec_extractFoldersTo",
  contexts: ["folder_pane"]
}, onCreated);

browser.menus.create({
  id: "aec_justDeleteFoldersAttachments",
  parentId: "aec_extractFoldersTo",
  title: browser.i18n.getMessage("justDeleteAttachments_label"),
  contexts: ["folder_pane"]
}, onCreated);
****************************************************************************/

// Callback (unused)
function onCreated() {
}

// Add eventListener to be able to refresh the _submenus_ for MRU and Favorite folders
/*
browser.menus.onShown.addListener(async function(info, tab) {
  if (info.menuItemId !== "aec_extractToFavoritefolders" && info.menuItemId !== "aec_extractToMRUfolders") 
    return;

  consoleDebug("AEC: sub menu opened = " + info.menuItemId);

  var menuInstanceId = nextMenuInstanceId++;
  lastMenuInstanceId = menuInstanceId;

  // Build the MRU and/or Favorite folders list
  //
  // await browser.menus.update(menuId, ...);
  //

  // must now perform the check
  if (menuInstanceId !== lastMenuInstanceId) {
    return;
  }
  browser.menus.refresh();
});
*/

// Set enabled/disabled status for context menu entry and toolbar button in 2+ steps
// Step 1.1: disable if folder is changed
browser.mailTabs.onDisplayedFolderChanged.addListener((tab, displayedFolder) => {
  consoleDebug("AEC: Folder changed: disable context menu entry and toolbar button");

  browser.messageDisplayAction.disable();
  browser.browserAction.disable();
  browser.menus.update("aec_extractMessagesTo", {
    visible: false
  });
});

// Step 1.2: disable if tab is changed
browser.tabs.onActivated.addListener((activeInfo) => {
  consoleDebug("AEC: Tab changed: disable context menu entry and toolbar button");

  browser.messageDisplayAction.disable();
  browser.browserAction.disable();
  browser.menus.update("aec_extractMessagesTo", {
    visible: false
  });
});

// Step 2: re-enable if really a message is displayed
// browser.messageDisplay.onMessagesDisplayed.addListener(async (tab) => {
browser.messageDisplay.onMessageDisplayed.addListener(async (tab, message) => {
  consoleDebug("AEC: Message displayed: enable context menu entry and toolbar button");

  // check for account type and don't enable UI for rss and news accounts
  let account = await messenger.accounts.get(message.folder.accountId);
  consoleDebug("AEC: account type = " + account.type);
  if (account.type == "news" || account.type == "rss")
    return;

  browser.messageDisplayAction.enable();
  browser.browserAction.enable();
  browser.menus.update("aec_extractMessagesTo", {
    visible: true
  });
});

// Additional case, when to show/hide the context menu entry
browser.menus.onShown.addListener(async (info, tab) => {
  consoleDebug("AEC: menus.onShown: info = " + info);

  // Maybe helpfull for the following operations
  // Load the list of selected messages in message/thread pane:
  // messages = await browser.mailTabs.getSelectedMessages(tab.id);
  // or this code line used in background.js:
  // messages = await info.selectedMessages;

  // check for account type and don't enable UI for rss and news accounts
  let message = await browser.messageDisplay.getDisplayedMessage(tab.id);
  consoleDebug("AEC: browser.messageDisplay.getDisplayedMessage(tab.id) = " + message);

  let messages = await browser.messageDisplay.getDisplayedMessages(tab.id);
  consoleDebug("AEC: browser.messageDisplay.getDisplayedMessages(tab.id) = " + messages);
  
  // When no message or multiple messages are selected, message = null.
  // So message is only available for ONE selected message.
  if(message) {
    let account = await messenger.accounts.get(message.folder.accountId);

    consoleDebug("AEC: menus.onShown: single message; account type = " + account.type);

    if (account.type == "news" || account.type == "rss")
      return;
  } else if (messages[0]) {
    let account = await messenger.accounts.get(messages[0].folder.accountId);

    consoleDebug("AEC: menus.onShown: multiple messages; account type = " + account.type);

    if (account.type == "news" || account.type == "rss")
      return;
  }

  browser.menus.update("aec_extractMessagesTo", {
    visible: true
  });
  // After menu.update we have to menu.refresh to get it visible in the already open menu
  browser.menus.refresh();
});

browser.menus.onHidden.addListener((info) => {
  consoleDebug("AEC: menus.onHidden: disable context menu entry");

  browser.menus.update("aec_extractMessagesTo", {
    visible: false
  });
});
